$(function() {

	handleResponsiveMenu();
	handleMenuLinks();
	$(window).on('resize', onResize);
	$(window).on('scroll', onScroll);
	$(window).trigger('resize');	

});

$(window).on("load", function() {
	$('body').css('opacity', 1);
});

function handleResponsiveMenu() {
	$('button.navbar-toggle').on('click', function() {
		if( $(this).hasClass('collapsed') ) {
			$('nav').addClass('with-bg');
		}
		else {
			setTimeout(function() {
				$('nav').removeClass('with-bg');
			}, 350);
		}
	});
}

function onResize() {
	var windowSize = getWindowSize();

	setKucharImageSize(windowSize);
	setFullscreenWrappersSize(windowSize);
	setFontsSize();
	setBurgerSize(windowSize);
	setSquarHolderHeight();
	owlSlider(windowSize);
	onScroll();
}

function setFontsSize() {

	var DEFAULT_WINDOW_WIDTH = 1200;

	// set Uvod font size
	if( $(window).width() > DEFAULT_WINDOW_WIDTH )
		$('#uvod-text').css('font-size', ($(window).width() / DEFAULT_WINDOW_WIDTH) + 'em' );
	else
		$('#uvod-text').css('font-size', '1em');
}

function setBurgerSize(windowSize) {
	var burgerWrapperHeight =	( $(window).width() > 1200 ) ? ($(window).width() / 4.8) : 250;
	var burgerImageHeight;

	if( windowSize.width > 480 ) {
		burgerImageHeight =		( $(window).width() > 1200 ) ? ($(window).width() / 2.264150943396226) : 530;
	}
	else {
		burgerImageHeight = 250;
	}

	$('#burger').css('height', parseInt(burgerWrapperHeight) + 'px');
	$('#burger > div').css('height', parseInt(burgerImageHeight) + 'px');

	if( windowSize.width >= 600  ) {
		$('#burger > h2').css('line-height', parseInt(burgerWrapperHeight) + 'px');
	}
	else {
		$('#burger > h2').css('line-height', '');
	}
}

function setFullscreenWrappersSize(windowSize) {
	$('.wrapper-fullscreen').css({
		height:	windowSize.height,
		width:	windowSize.width
	});

	if( windowSize.width < 768 && $('#uvod').height() < 640 ) {
		$('#uvod').css('height', '640px');
	}
}

function map( x,  in_min,  in_max,  out_min,  out_max) {
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

function onScroll() {
	var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
	var windowSize = getWindowSize();

	setTopMenuPosition(scrollTop, windowSize);
	handleBurgerParalax(scrollTop, windowSize);
}

function getWindowSize() {
	var windowSize = {
		width:	$(window).width(),
		height:	$(window).height()
	};

	return windowSize;
}

function setTopMenuPosition(scrollTop, windowSize) {
	var menuTopPosition;
	var menuHeight = 68;

	if( windowSize.width >= 768 ) {
		if( (scrollTop > (windowSize.height - menuHeight)) ) {
			menuTopPosition = map(scrollTop, (windowSize.height - menuHeight), windowSize.height, -menuHeight, 0);
			if( menuTopPosition > 0 )
				menuTopPosition = 0;

			$('nav.navbar').removeClass('navbar-absolute-top');
			$('nav.navbar').addClass('navbar-fixed-top');
			$('nav.navbar.navbar-fixed-top').css('transform', 'translate3d(0px, '+menuTopPosition+'px, 0px)');
		}
		else {
			$('nav.navbar').removeClass('navbar-fixed-top');
			$('nav.navbar').addClass('navbar-absolute-top');
			$('nav.navbar.navbar-absolute-top').css('transform', 'translate3d(0px, 25px, 0px)');
		}
	}
	else {
		$('nav.navbar').removeClass('navbar-fixed-top');
		$('nav.navbar').addClass('navbar-absolute-top');
	}
}

function setKucharImageSize(windowSize) {
	var kucharImageCss = {
		height: '',
		width: ''
	};

	if( windowSize.width >= 992 && 1110 > windowSize.width ) {
		kucharImageCss.width = windowSize.width * 0.5585585585585586 + 'px';
		kucharImageCss.height = 0.7258064516129032 * parseInt(kucharImageCss.width) + 'px';
		$('#kuchar-img').css(kucharImageCss);
	}
	else if (windowSize.width < 768) {
		$('#kuchar-img').css('width', 'auto');
		$('#kuchar-img').css('height', (1.022222222222222 * parseInt( $('#kuchar-img').css('width') ) ) + 'px');
	}
	else {
		$('#kuchar-img').css(kucharImageCss);
	}
}

function handleBurgerParalax(scrollTop, windowSize) {
	if( windowSize.width < 768 )
		return;

	var burgerHeight = document.getElementById('burger').childNodes[1].clientHeight;
	var burgerBgTop;
	var paralaxScrollDistance = windowSize.height;
	var paralaxStartHeight = document.getElementById('burger').offsetTop - windowSize.height;

	if( scrollTop > paralaxStartHeight - paralaxScrollDistance ) {

		burgerBgTop = map( scrollTop,  paralaxStartHeight + paralaxScrollDistance, paralaxStartHeight,  0, -burgerHeight/2);

		$('#burger > div').css('transform', 'translate3d(0px, ' + burgerBgTop + 'px, 0px)');
	}
	else {
		$('#burger > div').css('transform', 'translate3d(0px, 0px, 0px)');
	}
}

function scrollToElement(selector, duration, add) {

	if(!selector)
		return false;

	duration = duration || 1000;
	add = add || 0;

	$("html, body").animate({ scrollTop: $(selector).offset().top - add }, duration);
	return false;
}

function setSquarHolderHeight() {
	var windowWidth = $(window).width();
	var DEFAULT_WINDOW_WIDTH = (windowWidth > 767) ? 1200 : 480;
	var outerWidth = $('.square-holder').outerWidth();

	$('.square-holder').each(function() {
		$(this).css('height', $(this).outerWidth() + 'px');

		$(this).find('div').css('height', ($(this).outerHeight() - parseInt($('.square-holder').css('padding-top') ) *2 ) + 'px');
	});

	if( windowWidth < DEFAULT_WINDOW_WIDTH && windowWidth > 991 ) {
		$('#squares').css('font-size', (windowWidth / DEFAULT_WINDOW_WIDTH) * 1.1 + 'em');

		$('.square-holder > div > h3').css('margin-bottom', parseInt(outerWidth / 9) + 'px');
		$('.square-holder > div > span').css('bottom', parseInt(outerWidth / 10.3) + 'px');
	}
	else if( windowWidth < DEFAULT_WINDOW_WIDTH && windowWidth > 767 ) {
		$('#squares').css('font-size', (windowWidth / DEFAULT_WINDOW_WIDTH) * 1.2 + 'em');

		$('.square-holder > div').css('padding-top', parseInt(outerWidth / 15) + 'px');
		$('.square-holder > div > h3').css('margin-bottom', parseInt(outerWidth / 15) + 'px');
		$('.square-holder > div > span').css('font-size', outerWidth / 150 +'em') ;
		$('.square-holder > div > span').css('bottom', parseInt(outerWidth / 11) + 'px');
	}
	else if(windowWidth <= 767) {
		console.log(DEFAULT_WINDOW_WIDTH);
		$('#squares').css('font-size', (windowWidth / DEFAULT_WINDOW_WIDTH) * 1.2 + 'em');
		$('.square-holder > div > h3').css('margin-bottom', parseInt(outerWidth / 15) + 'px');
		$('.square-holder > div > span').css('font-size', outerWidth / 150 +'em') ;
		$('.square-holder > div > span').css('bottom', parseInt(outerWidth / 11) + 'px');
	}
	else {
		$('#squares').css('font-size', '1em');
		$('.square-holder > div > h3').css('margin-bottom', '40px');
		$('.square-holder > div > span').css('bottom', '35px');
	}
}

function owlSlider(windowSize) {

	$('#gallery .item div').css('height', windowSize.height);

	$("#gallery").owlCarousel({
		navigation : true,
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem:true,
		navigation: true,
		pagination: false
	});

	$('#gallery .owl-controls .owl-buttons div').html('');
}

function handleMenuLinks() {
	$('.navbar-nav > li > a').on('click', function() {

		var href = $(this).attr('href');
		var selector = $(this).data('scroll-to');
		var menuOffset = 68;
		var windowWidth = $(window).width();

		if( $(window).width() < 992 ) {
			menuOffset = (windowWidth < 768) ? 0 : 105;

			if (href == '#denne-menu') {
				if(windowWidth < 768) {
					selector = '#denne-menu';
				}
			}
			else if (href == '#speciality') {
				selector = '#specials';
				menuOffset = (windowWidth < 768) ? 50 : 150;
			}
		}

		scrollToElement( selector, false, menuOffset );

		return false;
	});
}
